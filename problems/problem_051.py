# Write a function that meets these requirements.

# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator

# Don't for get to import math!

# Python has a built-in module called math, which extends the list of mathematical functions. To use it, you must import the math module, like so:
import math

 https://www.w3schools.com/python/python_math.asp

def safe_divide(x, y):
    if y == 0:
        return math.inf # Returns a floating-point positive infinity https://www.w3schools.com/python/ref_math_inf.asp
    else:
        return x / y

print(safe_divide(2, 0))
