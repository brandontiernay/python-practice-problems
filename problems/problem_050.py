# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(numbers):
    half_length_numbers = len(numbers) / 2
    first_half_of_numbers = []
    second_half_of_numbers = []

    for i in range(len(numbers)):
        if i < half_length_numbers:
            first_half_of_numbers.append(numbers[i])
        else:
            second_half_of_numbers.append(numbers[i])

    return first_half_of_numbers, second_half_of_numbers

print(halve_the_list([1, 2, 3, 4, 5]))
