# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

# My notes:
# abs(): https://www.w3schools.com/python/ref_func_abs.asp Returns the absolute value of a number (positive)
# max(): https://www.w3schools.com/python/ref_func_max.asp Returns the item with the highest value, or the item with the highest value in an iterable.
# sorted(): https://www.w3schools.com/python/ref_func_sorted.asp Returns a sorted list of the specified iterable object.

def biggest_gap(numbers):
    gap = 0

    for i in range(len(numbers) - 1):
        current_num = numbers[i]
        next_num = numbers[i + 1]
        if abs(current_num - next_num) > gap:
            gap = abs(current_num - next_num)

    return gap

print(biggest_gap([1, 3, 100, 103, 106]))


# def biggest_gap(numbers):
#     max_num = max(numbers)
#     min_num = min(numbers)
#     diff = max_num - min_num
#     gap = abs(diff)

#     return gap

# print(biggest_gap([1, 3, 100, 103, 106]))
