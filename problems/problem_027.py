# Complete the max_in_list function to find the maximum value in a list.

# If the list is empty, then return None.

def max_in_list(values):

    if len(values) == 0:
        return None

    return max(values) #https://www.w3schools.com/python/ref_func_max.asp

print("The maximum value in this list is", max_in_list([0, 10, 100, 1000]))
