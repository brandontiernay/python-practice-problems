# Complete the check_password function that accepts a single parameter, the password to check.

# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it

# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    has_lowercase_lttr = False
    has_uppercase_lttr = False
    has_digit = False
    has_special_char = False
    has_more_than_five = len(password) > 5
    has_less_than_thirteen = len(password) < 13

    for i in range(len(password)):
        char = password[i]
        if char.isalpha and char.islower:
            has_lowercase_lttr = True

        if char.isalpha and char.isupper:
            has_uppercase_lttr = True

        if char.isdigit:
            has_digit = True

        if (char == "$" or char == "!" or char == "@"):
            has_special_char = True

    is_password_valid = has_lowercase_lttr and has_uppercase_lttr and has_digit and has_special_char and has_more_than_five and has_less_than_thirteen

    return is_password_valid

print(check_password("pW1@23"))
