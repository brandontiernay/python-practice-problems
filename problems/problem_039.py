# Complete the reverse_dictionary function which has a single parameter that is a dictionary. Return a new dictionary that has the original dictionary's values for its keys, and the original dictionary's keys for its values.

# Examples:
#   * input:  {}
#     output: {}

#   * input:  {"key": "value"}
#     output: {"value", "key"}

#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

# Note to self: Checkout all the Python Dictionary Methods at https://www.w3schools.com/python/python_ref_dictionary.asp.

def reverse_dictionary(dictionary):
    new_dict = {}

    for key in dictionary:
        val = dictionary[key] # We are temporarily assigning the value (because calling the key always spits out the value) of key to new variable val.
    new_dict[val] = key # This is standard way of adding key:value pairs to dictionary. In this instance, we are choosing to have the new key be the values of the old dictionary.
    return new_dict

print(reverse_dictionary({"one": 1, "two": 2, "three": 3}))

# Alternative, cleaner solution: Use .items() https://www.w3schools.com/python/ref_dictionary_items.asp.
def reverse_dictionary(dictionary):
    new_dict = {}
    for key, value in dictionary.items(): # The items method returns the key-value pairs of the dictionary, as tuples in a list.
        new_dict[value] = key
    return new_dict
