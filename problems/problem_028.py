# Complete the remove_duplicate_letters that takes a string parameter "s" and returns a string with all of the duplicates removed.

# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"

# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):

    if remove_duplicate_letters == 0:
        return ""

    new_str = ""
    for letter in s:
        if letter not in new_str:
            new_str = new_str + letter
    return new_str

print("The new string without duplicate letters is:", remove_duplicate_letters("abcabc"))
