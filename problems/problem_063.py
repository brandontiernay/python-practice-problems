# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem

# My notes:
# ord(): https://www.w3schools.com/python/ref_func_ord.asp or https://docs.python.org/3/library/functions.html#ord
# chr(): https://www.w3schools.com/python/ref_func_chr.asp or https://docs.python.org/3/library/functions.html#chr


def shift_letters(string):
   new_str = ""
   for i in range(len(string)):
      character = string[i]
      unicode = ord(character)
      if character == "Z":
         new_str = new_str + "A"
      elif character == "z":
         new_str = new_str + "a"
      else:
          new_str = new_str + chr(unicode + 1)

   return new_str

print(shift_letters("zap"))
