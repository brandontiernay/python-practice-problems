# Complete the count_letters_and_digits function which accepts a parameter s that contains a string and returns two values, the number of letters in the string and the number of digits in the string

# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1

# To test if a character c is a digit, you can use the c.isdigit() method to return True of False

# To test if a character c is a letter, you can use the c.isalpha() method to return True of False

# Remember that functions can return more than one value in Python. You just use a comma with the return, like this:

#   return value1, value2

# Definitions:
# isdigit() method returns True if all the characters are digits, otherwise False. Exponents, like ², are also considered to be a digit. https://www.w3schools.com/python/ref_string_isdigit.asp

# isalpha() method returns True if al the characters in the text are letters, otherwise False. https://www.w3schools.com/python/ref_string_isalpha.asp

# The c in c.isdigit() and c.isalpha() stands for character, which is what you're trying to determing the class type of.

# The way these differ from type() method, is that the type() method will only tell you what data class type of an element as a whole. But if you're trying to find the data class type of every character in an element, the previous two are the best way to go.


def count_letters_and_digits(s):
    num_lttrs = 0
    num_digits = 0

    for num in s:
        if num.isalpha():
            num_lttrs += 1
        elif num.isdigit():
            num_digits += 1
        else:
            return None
    return num_lttrs, num_digits

print(count_letters_and_digits("1a"))
