# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of desired characters in the result
#   * a padding character
# and turns the number into a string of the desired length by adding the padding character to the left of it

# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"

#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"

#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"

#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):

    if len(str(number)) < length:
        pad_needed = length - len(str(number))
        new_pad = str(pad) * pad_needed
        str_num = str(number)
        result = new_pad + str_num
        return result
    else:
        return str(number)

print(pad_left(1000, 3, "0"))
