# Complete the find_second_largest function which accepts a list of numerical values and returns the second largest in the list

# If the list of values is empty, the function should return None.

# If the list of values has only one value, the function should return None.

# Write out some pseudocode before trying to solve the  problem to get a good feel for how to solve it.

def find_second_largest(values):

    if len(values) <= 1:
        return None

    largest = 0
    second_largest = 0

    for num in values:
        if num > largest:
            second_largest = largest
            largest = num
        elif num < largest:
            if num > second_largest:
                second_largest = num
    return second_largest

print("The second largest value is:", find_second_largest([10, 1000, 100]))
# Note to self: Run through Thonny so that you can visualize and understand it.
