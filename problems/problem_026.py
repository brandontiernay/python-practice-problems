# Complete the calculate_grade function which accepts a list of numerical scores each between 0 and 100.

# Based on the average of the scores, the function returns
#   1) An "A" for an average greater than or equal to 90
#   2) A "B" for an average greater than or equal to 80 and less than 90
#   3) A "C" for an average greater than or equal to 70 and less than 80
#   4) A "D" for an average greater than or equal to 60 and less than 70
#   5) An "F" for any other average

def calculate_grade(values):

    grade = sum(values) / len(values)
    if grade >= 90:
        return "A"
    elif grade >= 80:
        return "B"
    elif grade >= 70:
        return "C"
    elif grade >= 60:
        return "D"
    else:
        return "F"

print("Your grade is", calculate_grade([72, 86, 90]))
