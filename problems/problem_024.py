# Complete the calculate_average function which accepts a list of numerical values and returns the average of the numbers.

# If the list of values is empty, the function should return None

def calculate_average(values):

    # This conditional is an "edge case". It is common practice to put edge cases at the top. Hence why I placed the if statement above the sum() function despite the instructions stating it first.
    if len(values) == 0:
        return None

    return sum(values) / len(values) # https://www.w3schools.com/python/ref_func_sum.asp

print("The average of the numbers is", calculate_average([])) # Added an empty list inside the argument. This way, I can add multiple values nested inside a single argument and not get an error.
