# Complete the gear_for_day function which returns a list of gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain "surfboard"

# Solution if translating instructions to the letter (same order):
def gear_for_day(is_workday, is_sunny):
    gear_needed = []

    if not is_sunny and is_workday:
        gear_needed.append("umbrella")

    if is_workday:
        gear_needed.append("laptop")

    if not is_workday:
        gear_needed.append("surfboard")

    return gear_needed

print("The gear for the day is", gear_for_day(True, False))

# Solution if translating instructions in a way that reads easier (different order from instructions):
def gear_for_day(is_workday, is_sunny):
    gear_needed = []

    if is_workday and not is_sunny:
        gear_needed.append("umbrella")

    if is_workday:
        gear_needed.append("laptop")

    if not is_workday:
        gear_needed.append("surfboard")

    return gear_needed

print("The gear for the day is", gear_for_day(False, False))
